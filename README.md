# Ansible docker image

## Build

```bash
docker build -t ansible .
```

## Run

```bash
docker run --rm -v $PWD:/workdir -ti ansible all -i localhost, -e ansible_connection=local -m command -a "ls -l"
```
