FROM python:3

RUN apt-get update && apt-get install -y \
    rsync \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /workdir

ENTRYPOINT ["/usr/local/bin/ansible"]
